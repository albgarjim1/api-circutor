# Technical test Circutor

<!-- Section for your links, references, etc. --->

[//]: # "References"
[logo]: https://via.placeholder.com/900x300/000000/FFFFFF/?text=project+logo
[shields-badge]: https://img.shields.io/badge/make%20your%20own%20badges-on%20shields.io-brightgreen.svg
[bibtex-wikipedia]: https://en.wikipedia.org/wiki/BibTeX
[go-official-site]: https://golang.org/doc/install
[docker-install]: https://docs.docker.com/install/
[open-api2-link]: https://swagger.io/specification/v2/
[golang-install]: https://golang.org/doc/install
[vscode-go]: https://marketplace.visualstudio.com/items?itemName=ms-vscode.Go
[vscode-prettier]: https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

<!-- Your project's logo --->

![Your project's logo][logo]

<!-- Your badges --->

[![shields.io badge][shields-badge]](https://shields.io)

<!-- One liner about your project --->

Technical test for circutor using Helsinki API

## Table of Contents

- [Technical test Circutor](#technical-test-circutor)
  - [Table of Contents](#table-of-contents)
  - [Introduction](#introduction)
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
      - [Run locally](#run-locally)
      - [Run with docker](#run-with-docker)
    - [Configuration](#configuration)
    - [Profiling](#profiling)
  - [Callable routes](#callable-routes)
  - [Example calls](#example-calls)
    - [List buildings](#list-buildings)
    - [Get building](#get-building)
    - [Get electricity measures of building](#get-electricity-measures-of-building)
  - [Tasks](#tasks)
  - [Shortcuts](#shortcuts)

## Introduction

The following documentation assumes previous knowledge of:

- Docker containers
- General API usage (call a route and  process a response)

## Getting Started

### Prerequisites

- Install golang from the reference [guide][golang-install] if you want to run it locally
- Install [docker][docker-install] from the official site
- Optional: vscode addons for golang
  - [Vscode][vscode-go] Golang addon
  - [Prettier][vscode-prettier] for code formatting

### Installation

- Run the command on the terminal to clone the project from the gitlab repository

   ```sh
    git clone https://gitlab.com/albgarjim1/api-circutor.git
   ```

#### Run locally

1. You will find a folder named api-circutor, navigate inside the folder with the command

   ```sh
   cd ./api-circutor`
   ```

2. Run this command to install all the necessary golang packages

   ```sh
   go get -v ./...
   ```

3. Optional: modify the `.env` variables


4. After completing the 3 previous steps, the project should be configured and ready to run, in order to launch it execute the following command on the root of the directory, which will start the server and will show the logs on the terminal

    ```sh
    go run ./src/app.go
    ```

#### Run with docker

1. To launch the container and have the system running, execute the following command on the terminal:

    ```sh
    docker-compose up --build -d
    ```

2. To monitor the system is running, execute the following command which will display the logs

    ```sh
    docker-compose logs -t -f
    ```

### Configuration

The project contains a .env file with the following variables:
```
PORT=7879
```
Port in which the application will be launched
```
URL_BUILDINGS=https://helsinki-openapi.nuuka.cloud/api/v1.0/Property/List
```
Url to retrieve the list of buildings
```
URL_ELECTRICITY=https://helsinki-openapi.nuuka.cloud/api/v1.0/EnergyData/Daily/ListByProperty
```
Url to retrieve the electricity measures for a given building code
```
URL_PARAMS_ELECTRICITY=?Record=PropertyCode&ReportingGroup=Electricity&StartTime=2021-01-01&EndTime=2021-12-31
```
Parameters of the url to retrieve the electricity measures for a given building code
```
DB_HOST=localhost
```
Postgresql host
```
DB_PORT=5430
```
Postgresql port
```
DB_USER=postgres
```
Postgresql database user
```
DB_PASSWORD=postgres
```
Postgresql database password
```
DB_NAME=postgres
```
Postgresql database name
```
DB_SSL_MODE=disable
```
Postgresql ssl mode
```
LOG_LEVEL=DEBUG
```
Log level, admits INFO or DEBUG
```
ENVIRONMENT=dev
```
Environment, affects the tables on the database that are read and written, admits `dev` or `pro`
```
RECREATE_DB=true
```
If this parameter is set to `true` it will delete and re-create the database tables
```
SERVER_LOGS_FOLDER= logs
```
Logs folder
```
SERVER_LOGS_FILE= logs/server_info.log
```
Logs file
```
CORS_ALLOWED_ORIGINS=http://localhost:3001
```
In case this api is being used by a frontend, it specifies from which ips it can be called from

### Profiling

This api incorporates pprof to create profiling reports, in order to do that
- Have `pprof` and `Graphviz` installed
- Execute the following command
  ```
  go tool pprof http://localhost:7879/api/v1/debug/pprof/heap
  ```
- This command will open a pprof console, type `png` in there and it will generate a profiling report image

## Callable routes

Those are the core resources of the REST API and the routes used to modify them

| Resource       | Route                                                 | Method | Information                                                  |
| :------------- | :---------------------------------------------------- | :----: | :----------------------------------------------------------- |
| list buildings | `/api/v1/buildings`                                   |  GET   | Returns the list of buildings.                               |
| building       | `/api/v1/buildings/<building_code>`                   |  GET   | Returns a buinding by its code.                              |
| electricity    | `/api/v1/buildings/<building_code>/electricity/daily` |  POST  | Returns the electricity measures for the specified building. |

## Example calls

### List buildings

Request: `GET` `<url>/api/v1/buildings?filter=<filter>`
- Admits `filter` parameter, if passed returns buildings that contain this parameter in the name (as a substring)

Example:
- Request: `GET`  `<url>/api/v1/buildings?filter=Hakaniemen`
- Response:
    ```json
    {
        "timestamp": "2022-06-17T11:20:21.832397908+02:00",
        "data": [
            {
                "locationName": "1000 Hakaniemen kauppahalli",
                "propertyName": "1000 Hakaniemen kauppahalli",
                "propertyCode": "091-011-9902-0101"
            },
            {
                "locationName": "2296 Lasihalli Hakaniemen torilla",
                "propertyName": "2296 Lasihalli Hakaniemen torilla",
                "propertyCode": "091-011-9902-0101"
            }
        ],
        "Content-Type": "application/json"
    }
    ```

### Get building

Request: `GET` `<url>/api/v1/buildings/<building_code>`

Example:
- Request: `GET` `<url>/api/v1/buildings/091-004-9902-0008`
- Response:
    ```json
    {
        "Content-Type": "application/json",
        "timestamp": "2022-06-17T11:37:15.832823049+02:00",
        "data": [
            {
                "locationName": "1001 Hietalahden kauppahalli",
                "propertyName": "1001 Hietalahden kauppahalli",
                "propertyCode": "091-004-9902-0008"
            }
        ]
    }
    ```

### Get electricity measures of building

Request: `GET` `<url>/api/v1/buildings/<building_code>/electricity/daily`
- Admits optional timestamp parameter `from`, if passed returns electricity measures after the timestamp
- Admits optional timestamp parameter  `to`, if passed returns electricity measures before the timestamp

Example:
- Request: `GET` `<url>/api/v1/buildings/091-011-9902-0101/electricity/daily?from=2021-01-01T00:00:00Z&to=2021-01-19T00:00:00Z`
    ```json
    {
        "timestamp": "2022-06-17T11:26:31.022102105+02:00",
        "data": [
            {
                "timestamp": "2021-01-01T00:00:00Z",
                "propertyCode": "091-011-9902-0101",
                "value": 1.19,
                "unit": "kWh"
            },
            {
                "timestamp": "2021-01-02T00:00:00Z",
                "propertyCode": "091-011-9902-0101",
                "value": 30.24,
                "unit": "kWh"
            },
            {
                "timestamp": "2021-01-03T00:00:00Z",
                "propertyCode": "091-011-9902-0101",
                "value": 1.16,
                "unit": "kWh"
            },
            ...
        ],
        "Content-Type": "application/json"
    }
    ```
## Tasks
- Create server skeleton
- Setup buildings database & API call to add buildings to database
- Setup electricity database & API call to add electricity measures to database
- Endpoint to retrieve buildings
- Endpoint to retrieve specific building
- Endpoint to retrieve electricity measures from building
- Add routes for profiling
- README
- Refactored logic on application and domain and created presenters
- Manual tests docker & local environment works fine
- Unit tests

## Shortcuts
- There is no primary key in db because building code is not unique
- On the creation of the database tables, the tables are dropped and re-created instead of updated with an upsert (this is due to the lack of a primary key explained above)
