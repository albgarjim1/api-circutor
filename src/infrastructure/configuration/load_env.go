package configuration

import (
	"os"

	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
)

func LoadConfig() {
	// this means we dont have a build flag for docker, which means we load the local .env file
	if os.Getenv("BUILD") == "" {
		err := godotenv.Load("./.env")
		if err != nil {
			log.Fatal("Error loading .env file")
		}
	}
	log.Info("File .env loaded correctly")
}
