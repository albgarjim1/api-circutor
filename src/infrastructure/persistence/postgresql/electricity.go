package postgresql

import (
	"context"
	"database/sql"
	"fmt"

	domain "go-api/src/domain"
	"os"
	"strings"

	"github.com/joho/godotenv"
)

func WrapperElectricityRepository() (domain.ElectricityRepository, error) {
	var repository domain.ElectricityRepository = ElectricityRepository{
		driver: GetPostgresConn(),
	}

	// this means we dont have a build flag for docker, which means we load the local .env file
	if os.Getenv("BUILD") == "" {
		err := godotenv.Load()
		if err != nil {
		}
	}

	return repository, nil
}

type ElectricityRepository struct {
	driver *sql.DB
}

func (r ElectricityRepository) InsertElectricity(_o []domain.ElectricityObj, code string) (string, error) {
	sqlStr := fmt.Sprintf("INSERT INTO %s_electricity (date, property_code, value, unit) VALUES ", os.Getenv("ENVIRONMENT"))
	vals := []interface{}{}

	for _, row := range _o {
		sqlStr += "(?, ?, ?, ?),"
		vals = append(vals, row.Timestamp, code, row.Value, row.Unit)
	}

	sqlStr = strings.TrimSuffix(sqlStr, ",")
	sqlStr = ReplaceSQL(sqlStr, "?")
	stmt, _ := r.driver.Prepare(sqlStr)

	_, err := stmt.Exec(vals...)
	if err != nil {
		return "", err
	}

	return "collection created", nil
}

func (r ElectricityRepository) FindByBuildingCode(_ctx context.Context, _buildingCode string, _dateFrom *string, _dateTo *string) (*[]domain.ElectricityObj, error) {
	sqlStr := fmt.Sprintf("SELECT date, property_code, value, unit FROM %s_electricity WHERE property_code='%s'", os.Getenv("ENVIRONMENT"), _buildingCode)
	if _dateFrom != nil {
		sqlStr += fmt.Sprintf("AND date >= '%s'", *_dateFrom)
	}
	if _dateTo != nil {
		sqlStr += fmt.Sprintf("AND date < '%s';", *_dateTo)
	}
	rows, err := r.driver.Query(sqlStr)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	electricity := []domain.ElectricityObj{}

	for rows.Next() {
		var b domain.ElectricityObj
		if err := rows.Scan(&b.Timestamp, &b.PropertyCode, &b.Value, &b.Unit); err != nil {
			return nil, err
		}
		electricity = append(electricity, b)
	}

	return &electricity, nil
}
