package postgresql

import (
	"database/sql"
	"os"

	_ "github.com/lib/pq"

	"fmt"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
)

var postgresOnce sync.Once
var client *sql.DB

func GetPostgresConn() *sql.DB {
	var err error
	if client != nil {
		return client
	}
	postgresOnce.Do(func() {
		dbTimeout := time.Millisecond * 100

		log.Info("Connecting to postgredb...")
		psqlInfo := fmt.Sprintf(
			"host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
			os.Getenv("DB_HOST"),
			os.Getenv("DB_PORT"),
			os.Getenv("DB_USER"),
			os.Getenv("DB_PASSWORD"),
			os.Getenv("DB_NAME"),
			os.Getenv("DB_SSL_MODE"))

		for {
			client, err = sql.Open("postgres", psqlInfo)
			if err != nil {
				panic(err)
			}

			err = client.Ping()
			if err != nil {
				log.Error("Database connection denied, attempting to reconnect after ", dbTimeout)
				time.Sleep(dbTimeout)
				dbTimeout *= 2
			} else {
				log.Info("Connection successful to postgresql")
				break
			}
		}
	})
	return client
}
