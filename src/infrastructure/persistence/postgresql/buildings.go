package postgresql

import (
	"context"
	"database/sql"
	"fmt"
	domain "go-api/src/domain"
	"os"
	"strconv"
	"strings"

	"github.com/joho/godotenv"
)

func WrapperBuildingRepository() (domain.BuildingRepository, error) {
	var repository domain.BuildingRepository = BuildingRepository{
		driver: GetPostgresConn(),
	}

	// this means we dont have a build flag for docker, which means we load the local .env file
	if os.Getenv("BUILD") == "" {
		err := godotenv.Load()
		if err != nil {
		}
	}

	return repository, nil
}

type BuildingRepository struct {
	driver *sql.DB
}

// ReplaceSQL replaces the instance occurrence of any string pattern with an increasing $n based sequence
func ReplaceSQL(old, searchPattern string) string {
	tmpCount := strings.Count(old, searchPattern)
	for m := 1; m <= tmpCount; m++ {
		old = strings.Replace(old, searchPattern, "$"+strconv.Itoa(m), 1)
	}
	return old
}

func (r BuildingRepository) InsertBuildings(_o []domain.BuildingObj) (string, error) {
	sqlStr := fmt.Sprintf("INSERT INTO %s_buildings (property_code, property_name, location_name) VALUES ", os.Getenv("ENVIRONMENT"))
	vals := []interface{}{}

	for _, row := range _o {
		sqlStr += "(?, ?, ?),"
		vals = append(vals, row.PropertyCode, row.PropertyName, row.LocationName)
	}

	sqlStr = strings.TrimSuffix(sqlStr, ",")
	sqlStr = ReplaceSQL(sqlStr, "?")
	stmt, _ := r.driver.Prepare(sqlStr)

	_, err := stmt.Exec(vals...)
	if err != nil {
		return "", err
	}

	return "collection created", nil
}

func (r BuildingRepository) FindAllBuildingCodes() (*[]domain.BuildingObj, error) {
	sqlStr := fmt.Sprintf("SELECT property_code FROM %s_buildings", os.Getenv("ENVIRONMENT"))
	rows, err := r.driver.Query(sqlStr)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	buildings := []domain.BuildingObj{}

	for rows.Next() {
		var b domain.BuildingObj
		if err := rows.Scan(&b.PropertyCode); err != nil {
			return nil, err
		}
		buildings = append(buildings, b)
	}

	return &buildings, nil
}

func (r BuildingRepository) FindAll(_ctx context.Context, _filterName *string) (*[]domain.BuildingObj, error) {

	sqlStr := fmt.Sprintf("SELECT property_code, property_name, location_name FROM %s_buildings", os.Getenv("ENVIRONMENT"))
	if _filterName != nil {
		sqlStr += fmt.Sprintf(" WHERE property_name like '%%%s%%'", *_filterName)
	}
	rows, err := r.driver.Query(sqlStr)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	buildings := []domain.BuildingObj{}

	for rows.Next() {
		var b domain.BuildingObj
		if err := rows.Scan(&b.PropertyCode, &b.PropertyName, &b.LocationName); err != nil {
			return nil, err
		}
		buildings = append(buildings, b)
	}

	return &buildings, nil
}

func (r BuildingRepository) FindByCode(_ctx context.Context, _propertyCode *string) (*[]domain.BuildingObj, error) {
	sqlStr := fmt.Sprintf("SELECT property_code, property_name, location_name FROM %s_buildings WHERE property_code='%s'", os.Getenv("ENVIRONMENT"), *_propertyCode)
	rows, err := r.driver.Query(sqlStr)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	buildings := []domain.BuildingObj{}

	for rows.Next() {
		var b domain.BuildingObj
		if err := rows.Scan(&b.PropertyCode, &b.PropertyName, &b.LocationName); err != nil {
			return nil, err
		}
		buildings = append(buildings, b)
	}

	return &buildings, nil
}
