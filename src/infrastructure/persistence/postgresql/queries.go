package postgresql

var (
	tableQueries = map[string]string{
		"dev": `
			CREATE TABLE if not exists dev_buildings (
				property_code VARCHAR(200) NOT NULL,
				property_name VARCHAR(200) NOT NULL,
				location_name VARCHAR(200) NOT NULL
			);
			CREATE TABLE if not exists dev_electricity (
				date TIMESTAMP NOT NULL,
				property_code VARCHAR(200) NOT NULL,
				value NUMERIC NOT NULL,
				unit VARCHAR(100) NOT NULL
			);
		`,
		"pro": `
			CREATE TABLE if not exists pro_buildings (
				property_code VARCHAR(200) NOT NULL,
				property_name VARCHAR(200) NOT NULL,
				location_name VARCHAR(200) NOT NULL
			);
			CREATE TABLE if not exists pro_electricity (
				date TIMESTAMP NOT NULL,
				property_code VARCHAR(200) NOT NULL,
				value NUMERIC NOT NULL,
				unit VARCHAR(100) NOT NULL
			);
		`,
	}
)
