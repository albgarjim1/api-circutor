package postgresql

import (
	"database/sql"
	"encoding/json"
	"fmt"
	domain "go-api/src/domain"
	"net/http"
	"os"
	"strconv"
	"time"

	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
)

func CreateDBStructure(client *sql.DB) {
	env := os.Getenv("ENVIRONMENT")
	query := tableQueries[env]
	if _, err := client.Exec(string(query)); err != nil {
		log.Error("Fatal error creating database structure")
		panic(err)
	}
}

func SetupDB(_rBuild domain.BuildingRepository, _rElec domain.ElectricityRepository) {
	client := GetPostgresConn()
	CreateDBStructure(client)

	recreate, err := strconv.ParseBool(os.Getenv("RECREATE_DB"))
	if err != nil {
		log.Error("Fatal error obtaining work envirnonment")
		panic(err)
	}

	if !recreate {
		return
	}

	// if recreate flag is set to true we drop the tables buildings and electricity and we re-create them
	env := os.Getenv("ENVIRONMENT")
	log.Info("Recreating tables")
	client.Exec(fmt.Sprintf(`DROP TABLE %s_buildings; DROP TABLE %s_electricity;`, env, env))
	CreateDBStructure(client)
	// we launch in a go routine so the server is responsive and can work while the db is filled
	go LoadDB(_rBuild, _rElec)
}

func LoadDB(_rBuild domain.BuildingRepository, _rElec domain.ElectricityRepository) {
	LoadBuildingData(_rBuild)
	LoadElectricityData(_rBuild, _rElec)
}

func LoadBuildingData(_rBuild domain.BuildingRepository) error {
	log.Info("Loading building data")

	buildings, err := APIGetBuildings()
	if err != nil {
		log.Error("Error getting list of buildings from external api: ", err)
		return err
	}

	_, err = _rBuild.InsertBuildings(buildings)
	if err != nil {
		log.Error("Error inserting buildings in database: ", err)
		return err
	}

	log.Info("Building data successfully loaded")
	return nil
}

type BuildingApi struct {
	LocationName string `json:"locationName" validate:"required"`
	PropertyName string `json:"propertyName" validate:"required"`
	PropertyCode string `json:"propertyCode" validate:"required"`
}

func APIGetBuildings() ([]domain.BuildingObj, error) {
	url := os.Getenv("URL_BUILDINGS")

	apiResp, err := http.Get(url)
	if err != nil {
		log.Error(err)
	}
	decodedResp := []BuildingApi{}

	if err := json.NewDecoder(apiResp.Body).Decode(&decodedResp); err != nil {
		log.Error("Error decoding body to building struct: ", err)
		return nil, err
	}

	domainResp := []domain.BuildingObj{}
	for _, b := range decodedResp {
		tmp := domain.BuildingObj{
			LocationName: b.LocationName,
			PropertyName: b.PropertyName,
			PropertyCode: b.PropertyCode,
		}
		domainResp = append(domainResp, tmp)
	}

	return domainResp, nil
}

func LoadElectricityData(_rBuild domain.BuildingRepository, _rElec domain.ElectricityRepository) error {
	var err error

	log.Info("Loading electricity data")
	buildings, err := _rBuild.FindAllBuildingCodes()
	if err != nil {
		log.Error("Error updating collection from database: ", err)
		return err
	}

	for _, b := range *buildings {
		electricity, err := APIGetElectricity(b.PropertyCode)
		if err != nil {
			continue
		}

		_, err = _rElec.InsertElectricity(electricity, b.PropertyCode)
		if err != nil {
			continue
		}
		time.Sleep(1 * time.Second)
	}
	if err != nil {
		log.Error("Error updating collection from database: ", err)
		return err
	}

	log.Info("Electricity data successfully loaded")
	return nil
}

type ElectricityApi struct {
	Timestamp    string  `json:"timestamp" validate:"required"`
	PropertyCode string  `json:"propertyCode" validate:"required"`
	Value        float64 `json:"value" validate:"required"`
	Unit         string  `json:"unit" validate:"required"`
}

func APIGetElectricity(propertyCode string) ([]domain.ElectricityObj, error) {
	url := fmt.Sprintf("%s%s&SearchString=%s", os.Getenv("URL_ELECTRICITY"), os.Getenv("URL_PARAMS_ELECTRICITY"), propertyCode)

	apiResp, err := http.Get(url)
	if err != nil {
		log.Error(err)
	}
	decodedResp := []ElectricityApi{}

	if err := json.NewDecoder(apiResp.Body).Decode(&decodedResp); err != nil {
		log.Error("Error decoding body to electricity struct: ", err)
		return nil, err
	}

	domainResp := []domain.ElectricityObj{}
	for _, m := range decodedResp {
		tmp := domain.ElectricityObj{
			Timestamp:    m.Timestamp,
			PropertyCode: m.PropertyCode,
			Value:        m.Value,
			Unit:         m.Unit,
		}
		domainResp = append(domainResp, tmp)
	}

	return domainResp, nil
}
