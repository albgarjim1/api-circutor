package domain

import "context"

type BuildingObj struct {
	LocationName string
	PropertyName string
	PropertyCode string
}

type BuildingRepository interface {
	FindByCode(_ctx context.Context, code *string) (*[]BuildingObj, error)
	FindAll(_ctx context.Context, _filterName *string) (*[]BuildingObj, error)
	InsertBuildings(_o []BuildingObj) (string, error)
	FindAllBuildingCodes() (*[]BuildingObj, error)
}
