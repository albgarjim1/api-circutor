package domain

import "context"

type ElectricityObj struct {
	ReportingGroup string
	LocationName   string
	Timestamp      string
	PropertyCode   string
	Value          float64
	Unit           string
}

type ElectricityRepository interface {
	FindByBuildingCode(_ctx context.Context, _buildingCode string, _dateFrom *string, _dateTo *string) (*[]ElectricityObj, error)
	InsertElectricity(_o []ElectricityObj, code string) (string, error)
}
