package application

import (
	"context"

	"go-api/src/domain"

	log "github.com/sirupsen/logrus"
)

type GetBuildingQuery struct {
	Code *string
}

func WrapperGetBuildingHandler(
	buildingRepo domain.BuildingRepository,
) GetBuilding {
	return GetBuilding{
		buildingRepo: buildingRepo,
	}
}

type GetBuilding struct {
	buildingRepo domain.BuildingRepository
}

func (c GetBuilding) Handle(_ctx context.Context, _q GetBuildingQuery) (*[]domain.BuildingObj, error) {
	buildings, err := c.buildingRepo.FindByCode(_ctx, _q.Code)
	if err != nil {
		log.Error("Error getting building from database: ", err)
		return nil, err
	}

	return buildings, nil
}
