package application

import (
	"context"

	"go-api/src/domain"

	log "github.com/sirupsen/logrus"
)

type GetElectricityQuery struct {
	BuildingCode string
	From         *string
	To           *string
}

type GetElectricityRepository struct {
	electricityRepo domain.ElectricityRepository
}

func WrapperGetElectricityHandler(
	electricityRepo domain.ElectricityRepository,
) GetElectricityRepository {
	return GetElectricityRepository{
		electricityRepo: electricityRepo,
	}
}

func (r GetElectricityRepository) Handle(_ctx context.Context, _q GetElectricityQuery) (*[]domain.ElectricityObj, error) {
	measures, err := r.electricityRepo.FindByBuildingCode(_ctx, _q.BuildingCode, _q.From, _q.To)
	if err != nil {
		log.Error("Error getting electricity measures from database: ", err)
		return nil, err
	}

	return measures, nil
}
