package application_test

import (
	"context"
	"errors"
	"go-api/src/application"
	"go-api/src/domain"
	mock "go-api/src/mock/domain"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestGetAllBuildings_Handle(t *testing.T) {
	t.Parallel()

	t.Run("It return list of all buildings without parameters", func(t *testing.T) {
		ctl := gomock.NewController(t)
		defer ctl.Finish()

		mockBuilding := mock.NewMockBuildingRepository(ctl)
		testEntity := []domain.BuildingObj{
			{
				LocationName: "location-name",
				PropertyName: "property-name",
				PropertyCode: "property-code",
			},
		}

		handler := application.WrapperGetAllBuildingsHandler(mockBuilding)
		ctx := context.TODO()

		query := application.GetAllBuildingsQuery{
			FilterName: nil,
		}

		mockBuilding.
			EXPECT().
			FindAll(ctx, nil).
			Return(&testEntity, nil).
			Times(1)

		_, err := handler.Handle(ctx, query)
		assert.NoError(t, err)
	})

	t.Run("It return list of all buildings with parameters", func(t *testing.T) {
		ctl := gomock.NewController(t)
		defer ctl.Finish()

		mockBuilding := mock.NewMockBuildingRepository(ctl)

		handler := application.WrapperGetAllBuildingsHandler(mockBuilding)
		ctx := context.TODO()

		filterName := "1001"
		query := application.GetAllBuildingsQuery{
			FilterName: &filterName,
		}

		mockBuilding.
			EXPECT().
			FindAll(ctx, &filterName).
			// Return(&expectedRide, nil).
			Times(1)

		_, err := handler.Handle(ctx, query)
		assert.NoError(t, err)
	})
	t.Run("It returns error if db query fails", func(t *testing.T) {
		ctl := gomock.NewController(t)
		defer ctl.Finish()

		mockBuilding := mock.NewMockBuildingRepository(ctl)

		handler := application.WrapperGetAllBuildingsHandler(mockBuilding)
		ctx := context.TODO()
		newErr := errors.New("new-error")

		filterName := "1001"
		query := application.GetAllBuildingsQuery{
			FilterName: &filterName,
		}

		mockBuilding.
			EXPECT().
			FindAll(ctx, &filterName).
			Return(nil, newErr).
			Times(1)

		_, err := handler.Handle(ctx, query)
		assert.Error(t, err)
	})
}
