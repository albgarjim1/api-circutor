package application_test

import (
	"context"
	"go-api/src/application"
	"go-api/src/domain"
	mock "go-api/src/mock/domain"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestGetElectricity_Handle(t *testing.T) {
	t.Parallel()

	t.Run("It returns electricity measures without optional params", func(t *testing.T) {
		ctl := gomock.NewController(t)
		defer ctl.Finish()

		mockElectricity := mock.NewMockElectricityRepository(ctl)
		testEntity := []domain.ElectricityObj{
			{
				ReportingGroup: "Electricity",
				LocationName:   "location",
				Timestamp:      "10-10-2022",
				PropertyCode:   "property-code",
				Value:          777,
				Unit:           "unit",
			},
		}

		handler := application.WrapperGetElectricityHandler(mockElectricity)
		ctx := context.TODO()

		query := application.GetElectricityQuery{
			BuildingCode: "100",
			From:         nil,
			To:           nil,
		}

		mockElectricity.
			EXPECT().
			FindByBuildingCode(ctx, query.BuildingCode, query.From, query.To).
			Return(&testEntity, nil).
			Times(1)

		_, err := handler.Handle(ctx, query)
		assert.NoError(t, err)
	})

	t.Run("It returns electricity measures with optional parameter 'from'", func(t *testing.T) {
		ctl := gomock.NewController(t)
		defer ctl.Finish()

		mockElectricity := mock.NewMockElectricityRepository(ctl)
		testEntity := []domain.ElectricityObj{
			{
				ReportingGroup: "Electricity",
				LocationName:   "location",
				Timestamp:      "10-10-2022",
				PropertyCode:   "property-code",
				Value:          777,
				Unit:           "unit",
			},
		}

		handler := application.WrapperGetElectricityHandler(mockElectricity)
		ctx := context.TODO()
		dateFrom := "2021-01-01T00:00:00Z"

		query := application.GetElectricityQuery{
			BuildingCode: "100",
			From:         &dateFrom,
			To:           nil,
		}

		mockElectricity.
			EXPECT().
			FindByBuildingCode(ctx, query.BuildingCode, query.From, query.To).
			Return(&testEntity, nil).
			Times(1)

		_, err := handler.Handle(ctx, query)
		assert.NoError(t, err)
	})

	t.Run("It returns electricity measures with optional parameter 'to'", func(t *testing.T) {
		ctl := gomock.NewController(t)
		defer ctl.Finish()

		mockElectricity := mock.NewMockElectricityRepository(ctl)
		testEntity := []domain.ElectricityObj{
			{
				ReportingGroup: "Electricity",
				LocationName:   "location",
				Timestamp:      "10-10-2022",
				PropertyCode:   "property-code",
				Value:          777,
				Unit:           "unit",
			},
		}

		handler := application.WrapperGetElectricityHandler(mockElectricity)
		ctx := context.TODO()
		dateTo := "2021-01-03T00:00:00Z"

		query := application.GetElectricityQuery{
			BuildingCode: "100",
			From:         nil,
			To:           &dateTo,
		}

		mockElectricity.
			EXPECT().
			FindByBuildingCode(ctx, query.BuildingCode, query.From, query.To).
			Return(&testEntity, nil).
			Times(1)

		_, err := handler.Handle(ctx, query)
		assert.NoError(t, err)
	})

	t.Run("It returns electricity measures with optional parameters 'from' and 'to'", func(t *testing.T) {
		ctl := gomock.NewController(t)
		defer ctl.Finish()

		mockElectricity := mock.NewMockElectricityRepository(ctl)
		testEntity := []domain.ElectricityObj{
			{
				ReportingGroup: "Electricity",
				LocationName:   "location",
				Timestamp:      "10-10-2022",
				PropertyCode:   "property-code",
				Value:          777,
				Unit:           "unit",
			},
		}

		handler := application.WrapperGetElectricityHandler(mockElectricity)
		ctx := context.TODO()
		dateFrom := "2021-01-01T00:00:00Z"
		dateTo := "2021-01-03T00:00:00Z"

		query := application.GetElectricityQuery{
			BuildingCode: "100",
			From:         &dateFrom,
			To:           &dateTo,
		}

		mockElectricity.
			EXPECT().
			FindByBuildingCode(ctx, query.BuildingCode, query.From, query.To).
			Return(&testEntity, nil).
			Times(1)

		_, err := handler.Handle(ctx, query)
		assert.NoError(t, err)
	})
}
