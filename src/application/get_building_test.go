package application_test

import (
	"context"
	"errors"
	"go-api/src/application"
	"go-api/src/domain"
	mock "go-api/src/mock/domain"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestGetBuilding_Handle(t *testing.T) {
	t.Parallel()

	t.Run("It returns a single building", func(t *testing.T) {
		ctl := gomock.NewController(t)
		defer ctl.Finish()

		mockBuilding := mock.NewMockBuildingRepository(ctl)
		testEntity := []domain.BuildingObj{
			{
				LocationName: "location-name",
				PropertyName: "property-name",
				PropertyCode: "property-code",
			},
		}

		handler := application.WrapperGetBuildingHandler(mockBuilding)
		ctx := context.TODO()

		query := application.GetBuildingQuery{
			Code: nil,
		}

		mockBuilding.
			EXPECT().
			FindByCode(ctx, nil).
			Return(&testEntity, nil).
			Times(1)

		_, err := handler.Handle(ctx, query)
		assert.NoError(t, err)
	})

	t.Run("It returns error if db query fails", func(t *testing.T) {
		ctl := gomock.NewController(t)
		defer ctl.Finish()

		mockBuilding := mock.NewMockBuildingRepository(ctl)

		handler := application.WrapperGetBuildingHandler(mockBuilding)
		ctx := context.TODO()
		newErr := errors.New("new-error")
		filterName := "1001"
		query := application.GetBuildingQuery{
			Code: &filterName,
		}

		mockBuilding.
			EXPECT().
			FindByCode(ctx, &filterName).
			Return(nil, newErr).
			Times(1)

		_, err := handler.Handle(ctx, query)
		assert.Error(t, err)
	})
}
