package application

import (
	"context"

	"go-api/src/domain"

	log "github.com/sirupsen/logrus"
)

type GetAllBuildingsQuery struct {
	FilterName *string
}

func WrapperGetAllBuildingsHandler(
	buildingRepo domain.BuildingRepository,
) GetAllBuildings {
	return GetAllBuildings{
		buildingRepo: buildingRepo,
	}
}

type GetAllBuildings struct {
	buildingRepo domain.BuildingRepository
}

func (c GetAllBuildings) Handle(_ctx context.Context, _q GetAllBuildingsQuery) (*[]domain.BuildingObj, error) {
	buildings, err := c.buildingRepo.FindAll(_ctx, _q.FilterName)
	if err != nil {
		log.Error("Error getting list of all buildings from database: ", err)
		return nil, err
	}

	return buildings, nil
}
