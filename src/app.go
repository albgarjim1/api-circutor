package main

import (
	log_setup "go-api/src/infrastructure/logging"
	http "go-api/src/ui/http"

	log "github.com/sirupsen/logrus"

	config "go-api/src/infrastructure/configuration"
	"os"
)

func main() {
	config.LoadConfig()
	log.Info("Rdb host and port:", os.Getenv("DB_PORT"))

	log_setup.InitializeLogger()

	s := http.Server{}
	s.Initialize(os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_HOST"), os.Getenv("DB_DATABASE"))
	s.Run(os.Getenv("PORT"))
}
