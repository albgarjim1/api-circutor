package presenters

type BuildingDto struct {
	LocationName string `json:"locationName" validate:"required"`
	PropertyName string `json:"propertyName" validate:"required"`
	PropertyCode string `json:"propertyCode" validate:"required"`
}
