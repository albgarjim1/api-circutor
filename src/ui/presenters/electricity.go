package presenters

type ElectricityDto struct {
	Timestamp    string  `json:"timestamp" validate:"required"`
	PropertyCode string  `json:"propertyCode" validate:"required"`
	Value        float64 `json:"value" validate:"required"`
	Unit         string  `json:"unit" validate:"required"`
}
