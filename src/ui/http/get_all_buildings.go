package http

import (
	"net/http"

	"go-api/src/application"
	"go-api/src/ui/presenters"

	log "github.com/sirupsen/logrus"
)

func (s *Server) GetAllBuildings(_w http.ResponseWriter, _r *http.Request) {
	query := application.GetAllBuildingsQuery{}

	filterName := _r.Context().Value("filter")
	if filterName != nil {
		tmp := filterName.(string)
		query.FilterName = &tmp
	}

	handler := application.WrapperGetAllBuildingsHandler(s.buildingRepo)

	response, err := handler.Handle(_r.Context(), query)
	if err != nil {
		log.Error("Error getting list of all buildings from database: ", err)
		RespondWithError(_w, ErrInternalServer)
		return
	}

	var presResp []presenters.BuildingDto
	for _, b := range *response {
		tmp := presenters.BuildingDto{
			LocationName: b.LocationName,
			PropertyName: b.PropertyName,
			PropertyCode: b.PropertyCode,
		}
		presResp = append(presResp, tmp)
	}
	RespondWithJSON(_w, http.StatusOK, presResp)
}
