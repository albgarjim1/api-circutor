package http

import (
	"database/sql"
	"errors"
	"net/http"
)

// AJG - TODO: check error messages

var (
	//ErrInternalServer is the error code for internal server error, this is the error returned by default if none of the errors match the one used
	ErrInternalServer = errors.New("Yep, server does not work :(")

	//ErrBodyParams is the error code for wrong parameters in the request body
	ErrBodyParams = errors.New("Invalid request payload")

	//ErrFetchResponse is the error for a response that fails using the implemented Fetch function
	ErrFetchResponse = errors.New("The fetch call did not return success")

	//ErrTooManyRequests is the error for when a user has performed too many requests and is rate limited
	ErrTooManyRequests = errors.New("Request limit exceeded, try again in a few seconds")

	//SuccessResource is the code for when a resource is created successfully
	SuccessResource = string("Resource created correctly")
)

/*
CodeMessage returns an http error and a message for a particular error
This function is called in RespondWithError to determine the user error message
*/
func CodeMessage(_err error) (int, string) {
	switch _err {
	case sql.ErrNoRows:
		return http.StatusNoContent, "No data found"

	case ErrBodyParams:
		return http.StatusBadRequest, _err.Error()

	case ErrInternalServer:
		return http.StatusInternalServerError, _err.Error()

	case ErrFetchResponse:
		return http.StatusNoContent, _err.Error()

	case ErrTooManyRequests:
		return http.StatusTooManyRequests, _err.Error()

	default:
		return http.StatusInternalServerError, "Yep, server fucked :("

	}
}
