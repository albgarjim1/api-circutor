package http

import (
	"net/http"
	"net/http/pprof"

	log "github.com/sirupsen/logrus"
)

const AV1 = "/api/v1"

func (s *Server) CreateRoutes() {
	s.Visitor.Handle("/buildings", http.HandlerFunc(s.GetAllBuildings)).Methods("GET")
	s.Visitor.Handle("/buildings/{code:[0-9-]+}", http.HandlerFunc(s.GetBuilding)).Methods("GET")
	s.Visitor.Handle("/buildings/{code:[0-9-]+}/electricity/daily", http.HandlerFunc(s.GetElectricity)).Methods("GET")

	// ============================= PROFILING ROUTES

	s.User.HandleFunc("/debug/pprof/", pprof.Index)
	s.User.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	s.User.HandleFunc("/debug/pprof/profile", pprof.Profile)
	s.User.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	s.User.HandleFunc("/debug/pprof/trace", pprof.Trace)

	s.User.Handle("/debug/pprof/block", pprof.Handler("block"))
	s.User.Handle("/debug/pprof/goroutine", pprof.Handler("goroutine"))
	s.User.Handle("/debug/pprof/heap", pprof.Handler("heap"))
	s.User.Handle("/debug/pprof/threadcreate", pprof.Handler("threadcreate"))
	log.Info("------------------------------------------------")
}
