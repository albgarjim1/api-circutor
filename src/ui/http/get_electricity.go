package http

import (
	"net/http"

	"go-api/src/application"
	"go-api/src/ui/presenters"

	log "github.com/sirupsen/logrus"
)

func (s *Server) GetElectricity(_w http.ResponseWriter, _r *http.Request) {
	query := application.GetElectricityQuery{}

	code := _r.Context().Value("code")
	if code == nil {
		log.Error("Error, query parameter building code is mandatory: ")
		RespondWithError(_w, ErrInternalServer)
		return
	}
	query.BuildingCode = code.(string)

	from := _r.Context().Value("from")
	if from != nil {
		tmp := from.(string)
		query.From = &tmp
	}

	to := _r.Context().Value("to")
	if to != nil {
		tmp := to.(string)
		query.To = &tmp
	}

	handler := application.WrapperGetElectricityHandler(s.electricityRepo)

	response, err := handler.Handle(_r.Context(), query)
	if err != nil {
		log.Error("Error getting building from database: ", err)
		RespondWithError(_w, ErrInternalServer)
		return
	}

	presResp := []presenters.ElectricityDto{}
	for _, m := range *response {
		tmp := presenters.ElectricityDto{
			Timestamp:    m.Timestamp,
			PropertyCode: m.PropertyCode,
			Value:        m.Value,
			Unit:         m.Unit,
		}
		presResp = append(presResp, tmp)
	}

	RespondWithJSON(_w, http.StatusOK, presResp)
}
