package http

import (
	"net/http"

	"go-api/src/application"
	"go-api/src/ui/presenters"

	log "github.com/sirupsen/logrus"
)

func (s *Server) GetBuilding(_w http.ResponseWriter, _r *http.Request) {
	query := application.GetBuildingQuery{}

	code := _r.Context().Value("code")
	if code != nil {
		tmp := code.(string)
		query.Code = &tmp
	}

	handler := application.WrapperGetBuildingHandler(s.buildingRepo)

	response, err := handler.Handle(_r.Context(), query)
	// response, err := application.GetBuildingHandler(_r.Context(), query)
	if err != nil {
		log.Error("Error getting building from database: ", err)
		RespondWithError(_w, ErrInternalServer)
		return
	}

	presResp := []presenters.BuildingDto{}
	for _, b := range *response {
		tmp := presenters.BuildingDto{
			LocationName: b.LocationName,
			PropertyName: b.PropertyName,
			PropertyCode: b.PropertyCode,
		}
		presResp = append(presResp, tmp)
	}
	RespondWithJSON(_w, http.StatusOK, presResp)
}
