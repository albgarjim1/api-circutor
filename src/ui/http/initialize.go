package http

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"go-api/src/domain"
	"go-api/src/infrastructure/persistence/postgresql"
	pdb "go-api/src/infrastructure/persistence/postgresql"
)

/*
Server contains all the necessary information to initialize and use the server

Router is the base router, it SHOULDN'T be used, create a subrouter instead, e.g. Visitor and User subrouters

DB is a pointer to a struct that contains the database models
*/
type Server struct {
	Router          *mux.Router
	Visitor         *mux.Router
	User            *mux.Router
	buildingRepo    domain.BuildingRepository
	electricityRepo domain.ElectricityRepository
}

func (s *Server) addUserMiddleware() {
	s.User.Use(RateLimiter)
	log.Info("Added RateLimiter middleware to user router")

	s.User.Use(ExtractParameters)
	log.Info("Added ExtractParameters middleware to user router")

	s.User.Use(SecureUser.Handler)
	log.Info("Added SecureUser middleware to user router")
}

func (s *Server) addVisitorMiddleware() {
	s.Visitor.Use(RateLimiter)
	log.Info("Added RateLimiter middleware to user router")

	s.Visitor.Use(ExtractParameters)
	log.Info("Added ExtractParameters middleware to visitor router")

	s.Visitor.Use(SecureVisitor.Handler)
	log.Info("Added SecureVisitor middleware to visitor router")
}

/*
Initialize does the server setup
*/
func (s *Server) Initialize(_user, _password, _dbHost string, _dbName string) {
	log.Info("Initializing go api")

	s.Router = mux.NewRouter()
	// for users
	s.Visitor = s.Router.PathPrefix(AV1).Subrouter()
	// for internal usage
	s.User = s.Router.PathPrefix(AV1).Subrouter()

	s.buildingRepo, _ = postgresql.WrapperBuildingRepository()
	s.electricityRepo, _ = postgresql.WrapperElectricityRepository()

	pdb.SetupDB(s.buildingRepo, s.electricityRepo)

	s.addVisitorMiddleware()
	s.addUserMiddleware()
	s.CreateRoutes()

	log.Info("Server initialized correctly")
}

func (s *Server) Run(_addr string) {
	var wait time.Duration
	flag.DurationVar(&wait, "graceful-timeout", time.Second*15, "the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m")
	flag.Parse()

	addrStr := fmt.Sprintf(":%s", _addr)
	log.Info("Server running on port: ", _addr)

	allowedHeaders := handlers.AllowedHeaders([]string{"Authorization", "Content-Type"})
	allowedOrigins := handlers.AllowedOrigins(strings.Split(os.Getenv("CORS_ALLOWED_ORIGINS"), ","))
	allowedCredentials := handlers.AllowCredentials()
	allowedMethods := handlers.AllowedMethods([]string{http.MethodGet, http.MethodPost, http.MethodPatch, http.MethodDelete})

	handler := handlers.CORS(allowedHeaders, allowedOrigins, allowedMethods, allowedCredentials)(s.Router)
	srv := &http.Server{
		Addr:    addrStr,
		Handler: handler,
		// Good practice: enforce timeouts for servers you create, it protects against slowloris attacks
		WriteTimeout: 150 * time.Second,
		ReadTimeout:  150 * time.Second,
		IdleTimeout:  time.Second * 600,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	srv.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Info("shutting down")
	os.Exit(0)
}
