package http

import (
	log "github.com/sirupsen/logrus"
)

var valid = map[string]func(string) interface{}{
	QFilterName:   valFilterName,
	QPropertyCode: valPropertyCode,
	QDateStart:    valDateStart,
	QDateEnd:      valDateEnd,
}

func valFilterName(_val string) interface{} {
	if _val == "" {
		log.Debug("Filtered filterName")
		return ""
	}
	return _val
}

func valPropertyCode(_val string) interface{} {
	if _val == "" {
		log.Debug("Filtered propertyCode")
		return ""
	}
	return _val
}

func valDateStart(_val string) interface{} {
	if _val == "" {
		log.Debug("Filtered propertyCode")
		return ""
	}
	return _val
}

func valDateEnd(_val string) interface{} {
	if _val == "" {
		log.Debug("Filtered propertyCode")
		return ""
	}
	return _val
}
