module go-api

go 1.17

require (
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	github.com/json-iterator/go v1.1.12
	github.com/lib/pq v1.10.6
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.3.0
	github.com/unrolled/secure v1.10.0
	golang.org/x/time v0.0.0-20220609170525-579cf78fd858
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/golang/mock v1.6.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
)
