# syntax=docker/dockerfile:1

##
## Multi-stage build, first we build the docker container
##
FROM golang:1.16-buster AS build

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .

RUN CGO_ENABLED=0 go build ./src/app.go

#
# Deploy here we copy the executable file together with the .env, we also create the necessary folders
#
FROM alpine:3.9.4
RUN apk --no-cache add ca-certificates

WORKDIR /app
# We copy the src folder to have access to the .sql schemas
COPY --from=build ./app/src ./src
COPY --from=build ./app/app .
COPY --from=build ./app/.env .

RUN mkdir logs
# RUN touch logs/server_info.log

ARG PORT
EXPOSE $PORT

CMD ["./app"]